package cat.itb.santiagoguzman7e5.cyclingrace

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cat.itb.santiagoguzman7e5.cyclingrace.databinding.ActivityMainBinding
import cat.itb.santiagoguzman7e5.cyclingrace.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}