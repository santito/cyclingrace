package cat.itb.santiagoguzman7e5.cyclingrace.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.SystemClock
import android.provider.Telephony.Mms.Part
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.Chronometer
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import cat.itb.santiagoguzman7e5.cyclingrace.R
import cat.itb.santiagoguzman7e5.cyclingrace.databinding.FragmentMainBinding
import cat.itb.santiagoguzman7e5.cyclingrace.model.Cyclist
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import nl.dionsegijn.konfetti.core.Party
import nl.dionsegijn.konfetti.core.Position
import nl.dionsegijn.konfetti.core.emitter.Emitter
import nl.dionsegijn.konfetti.xml.KonfettiView
import java.util.concurrent.TimeUnit


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private var _player1: SeekBar? = null
    private val player1 get() = _player1!!

    private var _player2: SeekBar? = null
    private val player2 get() = _player2!!

    private var _player3: SeekBar? = null
    private val player3 get() = _player3!!

    private var _timer: Chronometer? = null
    private val timer get() = _timer!!

    private var _execButton: Button? = null
    private val execButton get() = _execButton!!

    private var _resetButton: Button? = null
    private val resetButton get() = _resetButton!!

    private var _konfetti: KonfettiView? = null
    private val konfetti get() = _konfetti!!

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        _player1 = binding.player1
        _player2 = binding.player2
        _player3 = binding.player3
        _timer = binding.timer
        _execButton = binding.execButton
        _resetButton = binding.resetButton
        _konfetti = binding.konfetti

        viewModel = ViewModelProvider(this,
            MainViewModel.Factory(
            Triple(
                Cyclist(name = "Player 1", view = player1, colors = listOf(0xe44d44, 0x484a50)),
                Cyclist(name = "Player 2", view = player2, colors = listOf(0x003062, 0xECF0FF)),
                Cyclist(name = "Player 3", view = player3, colors = listOf(0x984061, 0xFFD9E2))
            )
        ))[MainViewModel::class.java]

        var pauseOffSet: Long = 0

        viewModel.raceInProgress.observe(viewLifecycleOwner){
            execButton.text = if (it) {
                timer.base = SystemClock.elapsedRealtime() - pauseOffSet
                timer.start()
                getString(R.string.pause)
            } else {
                timer.stop()
                pauseOffSet = SystemClock.elapsedRealtime() - timer.base
                when {
                    viewModel.players.value!!.first.isFinished -> {
                        theWinnerIs(viewModel.players.value!!.first)
                    }
                    viewModel.players.value!!.second.isFinished -> {
                        theWinnerIs(viewModel.players.value!!.second)
                    }
                    viewModel.players.value!!.third.isFinished -> {
                        theWinnerIs(viewModel.players.value!!.third)
                    }
                }
                getString(R.string.start)
            }
        }

        resetButton.setOnClickListener {
            timer.base = SystemClock.elapsedRealtime()
            pauseOffSet = 0
            viewModel.reset()
        }

        execButton.setOnClickListener {
            viewModel.changeProgress()
            viewModel.race()
        }

        return binding.root
    }

    private fun theWinnerIs(winner: Cyclist) {
        val party = Party(
            speed = 0f,
            maxSpeed = 30f,
            damping = 0.9f,
            spread = 360,
            colors = winner.colors,
            emitter = Emitter(duration = 100, TimeUnit.MILLISECONDS).max(100),
            position = Position.Relative(0.5, 0.3)
        )
        Toast.makeText(context, "The winner is ${winner.name}", Toast.LENGTH_SHORT).show()
        konfetti.start(party)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}


