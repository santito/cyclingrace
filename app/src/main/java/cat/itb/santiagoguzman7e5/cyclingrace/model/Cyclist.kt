package cat.itb.santiagoguzman7e5.cyclingrace.model

import android.widget.SeekBar

class Cyclist(
    val name: String,
    val view: SeekBar,
    val colors: List<Int>,
    val maxProgress: Int = 100,
    val progressIncrement: Int = 1,
    private val initialProgress: Int = 0
) {
    val progressDelayMillis: Long
        get() = (0L until 1000L).random()

    val isFinished
        get() = maxProgress == view.progress

    init {
        require(maxProgress > 0) { "maxProgress=$maxProgress; must be > 0" }
        require(progressIncrement > 0) { "progressIncrement=$progressIncrement; must be > 0" }
        view.setOnTouchListener { _, _ -> true }
        view.max = maxProgress
        view.progress = initialProgress
    }

    fun reset() {
        view.progress = initialProgress
    }
}
