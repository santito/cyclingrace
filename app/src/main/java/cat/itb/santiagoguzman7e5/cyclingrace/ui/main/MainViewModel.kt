package cat.itb.santiagoguzman7e5.cyclingrace.ui.main

import androidx.lifecycle.*
import cat.itb.santiagoguzman7e5.cyclingrace.model.Cyclist
import kotlinx.coroutines.*

class MainViewModel(triple: Triple<Cyclist, Cyclist, Cyclist>) : ViewModel() {
    private val viewModelJob = SupervisorJob()
    private val uiScope
        get() = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val player1 get() = players.value?.first
    private val player2 get() = players.value?.second
    private val player3 get() = players.value?.third

    fun reset() {
        player1?.reset()
        player2?.reset()
        player3?.reset()
        _raceInProgress.value = false
    }

    fun changeProgress() {
        _raceInProgress.value = _raceInProgress.value?.not()
    }

    fun race() = uiScope.launch {
        if (raceInProgress.value == true) {
                launch(Dispatchers.Main){
                    player2?.let { run(it) }
                }
                launch(Dispatchers.Main) {
                    player1?.let { run(it) }
                }
                launch(Dispatchers.Main) {
                    player3?.let { run(it) }
                }

        }
    }

    private suspend fun run(cyclist: Cyclist) {
            with(cyclist){
                while (view.progress < maxProgress && raceInProgress.value == true){
                    delay(this.progressDelayMillis)
                    view.progress += progressIncrement
                }
                if (view.progress == maxProgress){
                    _raceInProgress.value = false
                }
            }
    }

    private val _raceInProgress = MutableLiveData(false)
    val raceInProgress: LiveData<Boolean>
        get() =  _raceInProgress

    private val _players = MutableLiveData(triple)
    val players: LiveData<Triple<Cyclist,Cyclist,Cyclist>>
        get() = _players

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    class Factory(private val players: Triple<Cyclist,Cyclist,Cyclist>) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MainViewModel(players) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

}
